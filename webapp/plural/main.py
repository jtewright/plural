#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import json
import models
import webapp2
import urllib2
import urllib
import urlparse
import logging
import datetime
import time
from google.appengine.api import app_identity
from google.appengine.api import images
from google.appengine.ext import ndb
from google.appengine.api import urlfetch

class MainHandler(webapp2.RequestHandler):
  def get(self):
    self.response.write('Hello world!')

class TasksHandler(webapp2.RequestHandler):
  def get(self):
    self.response.write('all tasks')

class TaskImageHandler(webapp2.RequestHandler):
  def get(self, taskid):
    task = models.Task.get_by_id(taskid)
    self.response.headers['Content-Type'] = "image/jpeg"
    self.response.out.write(task.image_data)

class TaskHandler(webapp2.RequestHandler):

  def task_to_response(self, task, userid, following):
      server_url = self.request.headers['Host']

      if following == 1:
        follows = models.Follow.query().filter(models.Follow.userid == str(userid))
        if follows.get() == None:
          return "No follows"
        for follow in follows:
          if task.entity_id == follow.id:
            user = models.User.query().filter(models.User.id == task.userid).get()
            user.name = user.name

            entity = models.Entity.query().filter(models.Entity.id == task.entity_id).get()
            entity.name = entity.name
            entity.id = entity.id

            total_supporters = models.Support.query().filter(models.Support.id == task.key.id()).count()
            supporter = models.Support.query().filter(models.Support.id == task.key.id()).filter(models.Support.userid == str(userid)).get()
            if supporter:
              end_user_supported = 1
            else:
              end_user_supported = 0

            response = models.Response.query().filter(models.Support.id == task.key.id()).order(-models.Response.timestamp).get()
            if response == None:
              responsetext = None
              responsestatus = 'Hoping for response'
              responsetimestamp = None
            else:
              responsetext = response.text
              responsetimestamp = (time.mktime((response.timestamp).timetuple()))*1000
              if response.status == 2:
                responsestatus = 'CONSIDERING'
              elif response.status == 3:
                responsestatus = 'DONE'
              elif response.status == 1:
                responsestatus = 'NOT RIGHT NOW'

            task_time = (time.mktime((task.timestamp).timetuple()))*1000

            return {
                  'id': task.key.id(),
                  'title': task.title,
                  'description': task.description,
                  'userid': task.userid,
                  'user_name': user.name,
                  'image_path': 'http://' + server_url + '/images/' + task.key.id() + '.jpg',
                  'timestamp': task_time,
                  'latitude': task.lat,
                  'longitude': task.lng,
                  'total_supporters': total_supporters,
                  'response': responsetext,
                  'response_timestamp': responsetimestamp,
                  'response_status': responsestatus,
                  'entity_name': entity.name,
                  'entity_id': entity.id,
                  'end_user_supported': end_user_supported,
                  'icon': 'img/location.png'
                  }

      else:
        user = models.User.query().filter(models.User.id == task.userid).get()
        user.name = user.name

        entity = models.Entity.query().filter(models.Entity.id == task.entity_id).get()
        entity.name = entity.name
        entity.id = entity.id

        total_supporters = models.Support.query().filter(models.Support.id == task.key.id()).count()
        supporter = models.Support.query().filter(models.Support.id == task.key.id()).filter(models.Support.userid == str(userid)).get()
        if supporter:
          end_user_supported = 1
        else:
          end_user_supported = 0

        response = models.Response.query().filter(models.Support.id == task.key.id()).order(-models.Response.timestamp).get()
        if response == None:
          responsetext = None
          responsestatus = 'Hoping for response'
          responsetimestamp = None
        else:
          responsetext = response.text
          responsetimestamp = (time.mktime((response.timestamp).timetuple()))*1000
          if response.status == 2:
            responsestatus = 'CONSIDERING'
          elif response.status == 3:
            responsestatus = 'DONE'
          elif response.status == 1:
            responsestatus = 'NOT RIGHT NOW'

        task_time = (time.mktime((task.timestamp).timetuple()))*1000

        return {
              'id': task.key.id(),
              'title': task.title,
              'description': task.description,
              'userid': task.userid,
              'user_name': user.name,
              'image_path': 'http://' + server_url + '/images/' + task.key.id() + '.jpg',
              'timestamp': task_time,
              'latitude': task.lat,
              'longitude': task.lng,
              'total_supporters': total_supporters,
              'response': responsetext,
              'response_timestamp': responsetimestamp,
              'response_status': responsestatus,
              'entity_name': entity.name,
              'entity_id': entity.id,
              'end_user_supported': end_user_supported,
              'icon': 'img/location.png'
              }

  def get(self, taskid=None):
    logging.info('Starting get suggestions request')
    self.response.headers.add_header('Access-Control-Allow-Origin', '*')
    following = int(self.request.get('following'))
    userid = self.request.params.get('userid')
    entity = self.request.params.get('entity')
    justuser = str(self.request.params.get('justuser'))
    if entity != '0':
      tasks = models.Task.query().filter(models.Task.entity_id == entity)
      logging.info('Getting suggestions for entity %s', entity)
    else:
      if justuser != '0':
        tasks = models.Task.query().filter(models.Task.userid == justuser)
        logging.info('Getting suggestions for user %s', justuser)
      else:
        tasks = models.Task.query()
        if following == 1:
            logging.info('Getting suggestions for followed entities')
        else:
            logging.info('Getting all suggestions')
    self.response.headers['Content-Type'] = "application/json"
    self.response.out.write(
                  json.dumps([self.task_to_response(t, userid, following) for t in tasks])
                  )

  def post(self):
    # authenticate user
    unauth_userid = self.request.get('userid')
    unauth_token = self.request.get('token')
    user = models.User.query().filter(models.User.id == unauth_userid).get()
    if user and (user.access_token == unauth_token) or (user.web_access_token == unauth_token):
        pass
    else:
        logging.warning('User not authenticated')
        self.response.set_status(401)
        self.response.write("Not authorised")
        return

    logging.info('Starting to post new suggestion. User %s', user.id)

    # set entity
    entity = models.Entity.query()
    entity = models.Entity.query().filter(models.Entity.gPlacesID == self.request.get('entity_gPlacesID')).get()
    if entity == None:
      entity = models.Entity(id=str(self.request.get('entity_id')))
      entity.name = self.request.get('entity_name').encode('ascii')
      entity.gPlacesID = self.request.get('entity_gPlacesID')
      entity.vicinity = self.request.get('entity_vicinity')
      entity.lat = float(self.request.get('entity_lat'))
      entity.lng = float(self.request.get('entity_lng'))
      logging.info('New suggestion: new entity set')
    else:
      logging.info('New suggestion: entity already in datastore')

    # set task
    task = models.Task(id=str(self.request.get('id')))
    task.userid = user.id
    task.title = self.request.get('title').encode('ascii')
    task.description = self.request.get('description').encode('ascii')
    task.lng = float(self.request.get('lng'))
    task.lat = float(self.request.get('lat'))
    task.image_data = self.request.get('image')
    task.image_data = images.resize(task.image_data, 500, correct_orientation=1)
    task.entity_id = entity.id
    logging.info('New suggestion: task set')

    # set support
    support = models.Support(id=str(self.request.get('id')))
    support.userid = str(self.request.get('userid'))
    support.supported = int(self.request.get('supported'))
    logging.info('New suggestion: support set')

    # set follow
    follows = models.Follow.query().filter(models.Follow.id == entity.id)
    user_following = 0
    for follow in follows:
        if follow.userid == user.id:
            user_following = user_following + 1
            logging.info("New suggestion: user already following entity")
    if user_following == 0:
        follow = models.Follow(id=str(self.request.get('entity_id')))
        follow.userid = str(self.request.get('userid'))
        logging.info("New suggestion: user not following entity, creating follow")
    logging.info('New suggestion: follow set')

    suggestion_to_put = [task, entity, follow, support]

    # put
    try:
      suggestion = ndb.put_multi(suggestion_to_put)
    except Exception as e:
      logging.exception('New suggestion: FAILED')
      self.response.set_status(500)
      self.response.write("Failed")
      return

    self.response.headers.add_header('Access-Control-Allow-Origin', '*')
    self.response.headers['Content-Type'] = 'application/json'
    self.response.set_status(200)
    self.response.write("Success")
    logging.info('New suggestion: SUCCESS')
    return

  def options(self):
    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, content-type, accept'
    self.response.headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE'

class SuggestionHandler(webapp2.RequestHandler):

    def suggestion_response(self, task, requser):
        server_url = self.request.headers['Host']

        user = models.User.query().filter(models.User.id == task.userid).get()
        user.name = user.name

        entity = models.Entity.query().filter(models.Entity.id == task.entity_id).get()
        entity.name = entity.name
        entity.id = entity.id

        total_supporters = models.Support.query().filter(models.Support.id == task.key.id()).count()
        supporter = models.Support.query().filter(models.Support.id == task.key.id()).filter(models.Support.userid == str(requser)).get()
        if supporter:
          end_user_supported = 1
        else:
          end_user_supported = 0

        response = models.Response.query().filter(models.Support.id == task.key.id()).order(-models.Response.timestamp).get()
        if response == None:
          responsetext = None
          responsestatus = 'Hoping for response'
          responsetimestamp = None
        else:
          responsetext = response.text
          responsetimestamp = (time.mktime((response.timestamp).timetuple()))*1000
          if response.status == 2:
            responsestatus = 'CONSIDERING'
          elif response.status == 3:
            responsestatus = 'DONE'
          elif response.status == 1:
            responsestatus = 'NOT RIGHT NOW'

        task_time = (time.mktime((task.timestamp).timetuple()))*1000

        return {
              'id': task.key.id(),
              'title': task.title,
              'description': task.description,
              'user_name': user.name,
              'image_path': 'http://' + server_url + '/images/' + task.key.id() + '.jpg',
              'timestamp': task_time,
              'latitude': task.lat,
              'longitude': task.lng,
              'total_supporters': total_supporters,
              'response': responsetext,
              'response_timestamp': responsetimestamp,
              'response_status': responsestatus,
              'entity_name': entity.name,
              'entity_id': entity.id,
              'end_user_supported': end_user_supported,
              'icon': 'img/location.png'
              }

    def get(self):
        logging.info('/s Getting suggestion')
        self.response.headers.add_header('Access-Control-Allow-Origin', '*')
        task = models.Task.get_by_id(str(self.request.params.get('id')))
        requser = self.request.params.get('requser')
        self.response.headers['Content-Type'] = "application/json"
        self.response.out.write(json.dumps([self.suggestion_response(task, requser)]))

    def options(self):
      self.response.headers['Access-Control-Allow-Origin'] = '*'
      self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, content-type, accept'
      self.response.headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE'


class EntitiesHandler(webapp2.RequestHandler):

  def entity_to_response(self, entity, userid):
    server_url = self.request.headers['Host']

    followers = models.Follow.query().filter(models.Follow.id == str(entity.id)).count()

    followed = models.Follow.query().filter(models.Follow.id == str(entity.id)).filter(models.Follow.userid == str(userid)).get()
    if followed:
      user_followed = 1
    else:
      user_followed = 0

    if models.Task.query().filter(models.Task.entity_id == str(entity.id)):
      total_suggestions = models.Task.query().filter(models.Task.entity_id == str(entity.id)).count()
      tasks = models.Task.query().filter(models.Task.entity_id == str(entity.id))
      total_dones = 0
      total_responses = 0
      for task in tasks:
        if models.Response.query().filter(models.Response.id == task.key.id()).get():
          total_responses = models.Response.query().filter(models.Response.id == task.key.id()).count()
          response = models.Response.query().filter(models.Response.id == task.key.id()).get()
          if response.status == 3:
            total_dones = total_dones + 1

    else:
      total_suggestions = 0

    return {
            'id': entity.id,
            'gPlacesID': entity.gPlacesID,
            'name': entity.name,
            'vicinity': entity.vicinity,
            'user_followed': user_followed,
            'total_followers': followers,
            'total_suggestions': total_suggestions,
            'latitude': entity.lat,
            'longitude': entity.lng,
            'total_dones': total_dones,
            'total_responses': total_responses
            }

  def get(self):
    self.response.headers.add_header('Access-Control-Allow-Origin', '*')
    self.response.headers['Content-Type'] = 'application/json'
    userid = self.request.params.get('userid')
    if self.request.params.get('entity_id') == 'all':
      logging.info('Getting all entities')
      entities = models.Entity.query()
      self.response.headers['Content-Type'] = "application/json"
      self.response.out.write(
                  json.dumps([self.entity_to_response(e, userid) for e in entities])
                  )
    else:
      entity = models.Entity.query().filter(models.Entity.id == self.request.params.get('entity_id')).get()
      logging.info('Getting entity %s', entity)
      self.response.headers['Content-Type'] = "application/json"
      self.response.out.write(
                  json.dumps([self.entity_to_response(entity, userid)])
                  )

  def options(self):
    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, content-type, accept'
    self.response.headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE'

class FollowHandler(webapp2.RequestHandler):

  def entity_update_follow(self):
    server_url = self.request.headers['Host']
    user_followed = 1
    return {
          'user_followed': user_followed
          }

  def entity_update_unfollow(self):
    server_url = self.request.headers['Host']
    user_followed = 0
    return {
          'user_followed': user_followed
          }

  def get(self):
    self.response.headers.add_header('Access-Control-Allow-Origin', '*')

    unauth_userid = self.request.params.get('userid')
    unauth_token = self.request.params.get('token')
    user = models.User.query().filter(models.User.id == unauth_userid).get()
    if user and (user.access_token == unauth_token) or (user.web_access_token == unauth_token):
        pass
    else:
        logging.warning('User not authenticated')
        self.response.set_status(401)
        self.response.write("Not authorised")
        return

    entity = models.Entity.query()
    if models.Entity.query().filter(models.Entity.gPlacesID == self.request.get('gPlacesID')).get():

      if int(self.request.params.get('unfollow')) == 0:
        logging.info('New follow for entity already in datastore')
        # make sure we don't create duplicate follow
        follows = models.Follow.query().filter(models.Follow.id == self.request.params.get('entityid'))
        user_following = 0
        for follow in follows:
            if follow.userid == user.id:
                user_following = user_following + 1
                logging.error("New follow: user already following entity (this failsafe shouldn't be called - investigate)")
                self.response.headers['Content-Type'] = 'application/json'
                self.response.out.write(json.dumps(self.entity_update_follow()))
        if user_following == 0:
            entityid = str(self.request.params.get('entityid'))
            follow = models.Follow(id=str(entityid))
            follow.userid = str(self.request.params.get('userid'))
            try:
              followkey = follow.put()
              logging.info('New follow: SUCCESS')
            except Exception as e:
              logging.exception('New follow: FAILED')
              self.response.set_status(500)
              self.response.write("Failed")
              return
            self.response.headers['Content-Type'] = 'application/json'
            self.response.out.write(json.dumps(self.entity_update_follow()))

      elif int(self.request.params.get('unfollow')) == 1:
        logging.info('New unfollow for entity already in datastore')
        entityid = str(self.request.params.get('entityid'))
        userid = str(self.request.params.get('userid'))
        followToDelete = models.Follow.query().filter(models.Follow.id == entityid).filter(models.Follow.userid == userid).get()
        key = followToDelete.key
        followToDelete.key.delete();
        self.response.headers['Content-Type'] = 'application/json'
        self.response.out.write(json.dumps(self.entity_update_unfollow()))

    else:
      logging.info('New follow for entity not in datastore')
      entity = models.Entity(id=str(self.request.get('entityid')))
      entity.name = self.request.get('name').encode('ascii')
      entity.gPlacesID = self.request.get('gPlacesID')
      entity.vicinity = self.request.get('vicinity')
      entity.lat = float(self.request.get('latitude'))
      entity.lng = float(self.request.get('longitude'))
      try:
        entitykey = entity.put()
        logging.info('New follow: new entity added to datastore')
      except Exception as e:
        logging.exception('New follow: FAILED to add new entity to datastore')
        self.response.set_status(500)
        self.response.write("Failed")
        return
      entityid = str(self.request.params.get('entityid'))
      follow = models.Follow(id=str(entityid))
      follow.userid = str(self.request.params.get('userid'))
      try:
        followkey = follow.put()
        logging.info('New follow: SUCCESS after adding new entity to datastore')
      except Exception as e:
        logging.exception('New follow: FAILED after adding new entity to datastore')
        self.response.set_status(500)
        self.response.write("Failed")
        return
      self.response.headers['Content-Type'] = 'application/json'
      self.response.out.write(json.dumps(self.entity_update_follow()))

  def options(self):
    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, content-type, accept'
    self.response.headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE'

class SupportHandler(webapp2.RequestHandler):

  def support_update_add(self, task_to_update):
    server_url = self.request.headers['Host']
    total_supporters = models.Support.query().filter(models.Support.id == str(task_to_update)).count()
    end_user_supported = 1
    return {
          'total_supporters': total_supporters,
          'end_user_supported': end_user_supported
          }

  def support_update_sub(self, task_to_update):
    server_url = self.request.headers['Host']
    total_supporters = models.Support.query().filter(models.Support.id == str(task_to_update)).count()
    end_user_supported = 0
    return {
          'total_supporters': total_supporters,
          'end_user_supported': end_user_supported
          }

  def get(self):
    self.response.headers.add_header('Access-Control-Allow-Origin', '*')

    unauth_userid = self.request.params.get('userid')
    unauth_token = self.request.params.get('token')
    user = models.User.query().filter(models.User.id == unauth_userid).get()
    if user and (user.access_token == unauth_token) or (user.web_access_token == unauth_token):
        pass
    else:
        logging.warning('User not authenticated')
        self.response.set_status(401)
        self.response.write("Not authorised")
        return

    if int(self.request.params.get('delete')) == 0:
      logging.info('New support')
      task_to_update = self.request.params.get('id')
      supporters = models.Support.query().filter(models.Support.id == task_to_update)
      user_supported = 0
      for supporter in supporters:
          if supporter.userid == self.request.params.get('userid'):
              user_supported = 1
              logging.warning('New support: tried to add double support')
      if user_supported == 0:
          support = models.Support(id=str(task_to_update))
          support.userid = str(self.request.params.get('userid'))
          support.supported = 1
          try:
            supportkey = support.put()
            logging.info('New support: SUCCESS')
          except Exception as e:
            logging.exception('New support: FAILED')
            self.response.set_status(500)
            self.response.write("Failed")
            return
      self.response.headers['Content-Type'] = 'application/json'
      self.response.out.write(json.dumps(self.support_update_add(task_to_update)))

    elif int(self.request.params.get('delete')) == 1:
      logging.info('New un-support')
      task_to_update = self.request.params.get('id')
      userid = str(self.request.params.get('userid'))
      supportToDelete = models.Support.query().filter(models.Support.id == task_to_update).filter(models.Support.userid == userid).get()
      if supportToDelete != None:
          try:
              supportToDelete.key.delete()
              logging.info('New un-support: SUCCESS')
          except Exception as e:
              logging.exception('New un-support: FAILED')
              self.response.set_status(500)
              self.response.write("Failed")
              return
      else:
          logging.warning('New un-support: tried to un-support twice')
      self.response.headers['Content-Type'] = 'application/json'
      self.response.out.write(json.dumps(self.support_update_sub(task_to_update)))


  def options(self):
    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, content-type, accept'
    self.response.headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE'

class PlacesHandler(webapp2.RequestHandler):

  def places_merge(self, place, userid):
    server_url = self.request.headers['Host']

    if models.Entity.query().filter(models.Entity.gPlacesID == place.get('id')).get():
      matched = models.Entity.query().filter(models.Entity.gPlacesID == place.get('id')).get()
      id = matched.id
      total_suggestions = models.Task.query().filter(models.Task.entity_id == matched.id).count()
      followed = models.Follow.query().filter(models.Follow.id == matched.id).filter(models.Follow.userid == str(userid)).get()
      if followed:
        user_followed = 1
      else:
        user_followed = 0
    else:
      total_suggestions = 0
      user_followed = 0
      id = None

    return {
          'name': place.get('name'),
          'vicinity': place.get('vicinity'),
          'address': place.get('formatted_address'),
          'gPlacesID': place.get('id'),
          'latitude': place.get('geometry').get('location').get('lat'),
          'longitude': place.get('geometry').get('location').get('lng'),
          'total_suggestions': total_suggestions,
          'user_followed': user_followed,
          'id': id
          }

  def get(self):
    logging.info('Getting places')
    userid = self.request.params.get('userid')
    query = self.request.params.get('query')
    location = self.request.params.get('location')
    #search
    if query:
        query = ("&query=" + self.request.params.get('query')).replace(" ", "%20")
        option = "&radius=50000"
        url = "https://maps.googleapis.com/maps/api/place/textsearch/json?location=" + location + query + option + '&key=AIzaSyC8ngjV5WbxSQSzt4fbxZxi_9tXRO-IapY'
        logging.info('Getting places: search')
    #list
    else:
        option = "&radius=50"
        url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=" + location + option + '&key=AIzaSyC8ngjV5WbxSQSzt4fbxZxi_9tXRO-IapY'
        logging.info('Getting places: nearby')
    try:
      result = urlfetch.fetch(url)
      self.response.headers.add_header('Access-Control-Allow-Origin', '*')
      places = json.loads(result.content)
      places = places.get('results')
      self.response.headers['Content-Type'] = 'application/json'
      self.response.out.write(
                json.dumps([self.places_merge(p, userid) for p in places])
                )
    except urllib2.URLError, e:
      handleError(e)
      logging.error('Getting places: FAILED')
      self.response.set_status(500)
      self.response.write("Failed")
      return

  def options(self):
    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, content-type, accept'
    self.response.headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE'

class UserHandler(webapp2.RequestHandler):

  def user_return(self, userid):
    server_url = self.request.headers['Host']
    user = models.User.query().filter(models.User.id == userid).get()
    total_suggestions = models.Task.query().filter(models.Task.userid == userid).count()
    suggestions = models.Task.query().filter(models.Task.userid == userid)
    total_responses = 0
    total_supports = 0
    if suggestions.get():
      for suggestion in suggestions:
        responses = models.Response.query().filter(models.Response.id == suggestion.key.id()).count()
        supports = models.Support.query().filter(models.Support.id == suggestion.key.id()).count()
        total_supports = total_supports + supports
        total_responses = total_responses + responses

    return {
            'name': user.name,
            'total_suggestions': total_suggestions,
            'total_responses': total_responses,
            'total_supports': total_supports,
          }

  def get(self):
    userid = self.request.params.get('userid')
    logging.info('Getting user details for %s', userid)
    self.response.headers.add_header('Access-Control-Allow-Origin', '*')
    self.response.headers['Content-Type'] = 'application/json'
    self.response.out.write(json.dumps([self.user_return(userid)]))

  def options(self):
    self.response.headers['Access-Control-Allow-Origin'] = '*'
    self.response.headers['Access-Control-Allow-Headers'] = 'Origin, X-Requested-With, content-type, accept'
    self.response.headers['Access-Control-Allow-Methods'] = 'POST, GET, PUT, DELETE'

class SuccessfulFbLoginHandler(webapp2.RequestHandler):

    def get(self):
        logging.info('New login')
        web_login = False
        self.response.headers.add_header('Access-Control-Allow-Origin', '*')
        server_url = self.request.headers['Host']
        if self.request.headers.get('Referer'):
            referer = self.request.headers['Referer']
            referer = referer.split('?')
            referer = referer[0]
            web_login = True
            logging.info('New login: web login')
        else:
            referer = "https://www.facebook.com/connect/login_success.html"
            logging.info('New login: app login')
        code = self.request.params.get('code')
        result = urlfetch.fetch('https://graph.facebook.com/v2.2/oauth/access_token?client_id=478193915690433' +
        '&redirect_uri=' + referer + '&client_secret=edf367dc337fef6780aede1a6d2c0d2d' +
        '&code=' + code)
        params = urlparse.parse_qs(result.content)
        access_token = params['access_token'][0]
        fb_user = urlfetch.fetch('https://graph.facebook.com/me?fields=id,name,email&access_token=' + access_token)
        fb_user = json.loads(fb_user.content)
        userid = fb_user.get('id')
        user = models.User.query().filter(models.User.id == userid).get()
        if user:
            if web_login == True:
                user.web_access_token = access_token
            else:
                user.access_token = access_token
            userkey = user.put()
            logging.info('New login: access_token updated for known user %s', userid)
        else:
            user = models.User(id=(userid))
            user.name = fb_user.get('name').encode('ascii')
            user.email = fb_user.get('email').encode('ascii')
            if web_login == True:
                user.web_access_token = access_token
            else:
                user.access_token = access_token
            userkey = user.put()
            logging.info('New login: new user created %s', userid)

        self.response.write(access_token)
        return


app = webapp2.WSGIApplication([
    ('/', MainHandler),
    ('/tasks', TaskHandler),
    ('/suggestion', SuggestionHandler),
    ('/entities', EntitiesHandler),
    ('/support', SupportHandler),
    ('/follow', FollowHandler),
    ('/user', UserHandler),
    ('/successfulFbLogin', SuccessfulFbLoginHandler),
    ('/places', PlacesHandler),
    ('/tasks/([\w-]+)', TaskHandler),
    ('/images/([\w-]+).jpg', TaskImageHandler)
], debug=True)

from google.appengine.ext import ndb

class Task(ndb.Model):
  userid = ndb.StringProperty()
  title = ndb.StringProperty()
  description = ndb.StringProperty()
  lng = ndb.FloatProperty()
  lat = ndb.FloatProperty()
  image_data = ndb.BlobProperty()
  timestamp = ndb.DateTimeProperty(auto_now_add=True)
  entity_id = ndb.StringProperty() #entity.id

class User(ndb.Model):
  id = ndb.StringProperty() #FB id
  name = ndb.StringProperty()
  email = ndb.StringProperty()
  timestamp = ndb.DateTimeProperty(auto_now_add=True)
  access_token = ndb.StringProperty()
  web_access_token = ndb.StringProperty()

class Support(ndb.Model):
  id = ndb.StringProperty() #task.key.id
  userid = ndb.StringProperty() #user.id
  supported = ndb.IntegerProperty() # 1 for yes
  timestamp = ndb.DateTimeProperty(auto_now_add=True)

class Response(ndb.Model):
  id = ndb.StringProperty() #task.key.id
  text = ndb.StringProperty()
  status = ndb.IntegerProperty() # 1 = not right now, 2 = considering, 3 = done
  timestamp = ndb.DateTimeProperty(auto_now_add=True)

class Entity(ndb.Model):
  id = ndb.StringProperty() #task.entity_id
  name = ndb.StringProperty()
  gPlacesID = ndb.StringProperty()
  vicinity = ndb.StringProperty()
  lat = ndb.FloatProperty()
  lng = ndb.FloatProperty()
  timestamp = ndb.DateTimeProperty(auto_now_add=True)

class Follow(ndb.Model):
  id = ndb.StringProperty() #entity.id
  userid = ndb.StringProperty() #user.id
  timestamp = ndb.DateTimeProperty(auto_now_add=True)

angular.module('Plural', ['ngRoute', 'ngOpenFB'])

.config(function($routeProvider, $locationProvider) {
  $routeProvider
  .when('/s', {
    templateUrl: '/s/index.html',
    controller: 'SuggestionCtrl'
  });
  $locationProvider.html5Mode(true);
  $locationProvider.hashPrefix('!');
})

.run(function(ngFB, Config) {
  ngFB.init({appId: '478193915690433'});
})

.factory('Config', function() {
    return {
        service_hostname: 'http://plural-960.appspot.com'
    }
})

.factory('Utils', function() {
    return {
      getDateString: function(ts) {
                          var minutes = 1000 * 60;
                          var hours = minutes * 60;
                          var days = hours * 24;
                          var weeks = days * 7;
                          var years = days * 365;

                          var now = new Date();

                          var date = new Date(ts);

                          var difference = (now - date);

                          var minsAgo = Math.round(difference / minutes);
                          var hoursAgo = Math.round(difference / hours);
                          var daysAgo = Math.round(difference / days);
                          var weeksAgo = Math.round(difference / weeks);
                          var yearsAgo = Math.round(difference / years);

                          var dd = date.getDate();
                          var month = new Array();
                              month[0] = "Jan";
                              month[1] = "Feb";
                              month[2] = "Mar";
                              month[3] = "Apr";
                              month[4] = "May";
                              month[5] = "Jun";
                              month[6] = "Jul";
                              month[7] = "Aug";
                              month[8] = "Sep";
                              month[9] = "Oct";
                              month[10] = "Nov";
                              month[11] = "Dec";
                          var mm = month[date.getMonth()];
                          var yy = date.getFullYear();

                          /* return dd + " " + mm; */

                          if (yearsAgo >= 1) {
                            return yearsAgo + "y";
                          }
                          else if (weeksAgo >= 1) {
                            return weeksAgo + "w";
                          }
                          else if (daysAgo >= 1) {
                            return daysAgo + "d";
                          }
                          else if (hoursAgo >= 1) {
                            return hoursAgo + "h";
                          }
                          else if (minsAgo >= 1) {
                            return minsAgo + "m";
                          }
                          else {
                            return "1m"
                          }
                     },
    }
})

.service('Suggestions', function($http, $q, Config, Utils) {
    return {
      getSuggestion: function(id, requser, suggestion) {
        var handleSuccess = function(resp) {
                    return suggestion(resp.data);
                }
                var handleError = function(resp) {
                    return resp.data;
                }
                var request = $http({
                    method: "get",
                    url: Config.service_hostname + "/suggestion" + "?" + "id=" + [id] + "&" + "requser=" + [requser]
                });
                return (request.then(handleSuccess, handleError));
      },
      Support: function(id, userid, token, end_user_supported, supporttime, supporters) {
          var request = $http({
              method: "get",
              url: Config.service_hostname + "/support" + "?" +
              "id=" + [id] +
              "&" + "userid=" + [userid] +
              "&" + "token=" + [token] +
              "&" + "delete=" + [end_user_supported] +
              "&" + "supporttime=" + [supporttime]
          });
          var handleSuccess = function(resp) {
              return supporters(resp.data);
          }
          var handleError = function(resp) {
              return supporters(resp.data);
          }
          return (request.then(handleSuccess, handleError));
      }
    }


})
.controller('SuggestionCtrl', function($scope, ngFB, Suggestions, Utils, $location) {

  $scope.loginToSupport = false;
  $scope.user = $scope.user || {};
  var suggestionid = $location.search().id;
  $scope.getDateString = Utils.getDateString;
  var loginWithSupport = 0;
  $scope.message = true;

  $scope.fbLogin = function () {
    ngFB.login({scope: 'email'}).then(
        function (response) {
            if (response.status === 'connected') {
                $scope.loggedIn = 1;
                $scope.message = true;
                loadUser();
            } else {
                alert('Facebook login failed');
            }
        });
  };

  $scope.fbLoginSupport = function () {
    ngFB.login({scope: 'email'}).then(
        function (response) {
            if (response.status === 'connected') {
                loginWithSupport = 1;
                $scope.loggedIn = 1;
                $scope.message = true;
                loadUser();
            } else {
                alert('Facebook login failed');
            }
        });
  };

  $scope.fbLogout = function () {
    fbLogout();
  }

  var fbLogout = function () {
    ngFB.logout()
    localStorage.removeItem('fbAccessToken');
    $scope.loggedIn = 0;
    $scope.user = 'None';
    loadSuggestion();
  }

  var loadUser = function () {
    ngFB.api({
        path: '/me',
        params: {fields: 'id,name,email'}
    }).then(
        function (user) {
            $scope.user = user;
            loadSuggestion();
        },
        function (error) {
          $scope.loggedIn = 0;
          $scope.user.id = 0;
          loadSuggestion();
        });
  }

  var loadSuggestion = function () {
      Suggestions.getSuggestion(suggestionid, $scope.user.id, function(suggestion) {
      $scope.suggestion = suggestion;
      if (loginWithSupport == 1 && $scope.suggestion[0].end_user_supported == 0) {
        Support($scope.suggestion[0]);
      }
      loginWithSupport = 0;
    });
  }

  if (localStorage.fbAccessToken) {
    loadUser();
    $scope.loggedIn = 1;
  }
  else {
    $scope.loggedIn = 0;
    $scope.user.id = 0;
    loadSuggestion();
  }

  var Support = function(suggestion) {
    supporttime = new Date().getTime();
    Suggestions.Support(suggestion.id, $scope.user.id, localStorage.fbAccessToken, suggestion.end_user_supported, supporttime, function(supporters) {
      if (supporters == "Not authorised") {
        alert("Sorry, there was a problem. Please login and try again :-)");
        fbLogout();
      }
      else if (suggestion.end_user_supported == 1) {
        suggestion.total_supporters = suggestion.total_supporters - 1;
        suggestion.end_user_supported = 0;
      }
      else if (suggestion.end_user_supported == 0) {
        suggestion.total_supporters = suggestion.total_supporters + 1;
        suggestion.end_user_supported = 1;
      }
    });
  }

  $scope.Support = function(suggestion) {
    Support(suggestion);
   }

});

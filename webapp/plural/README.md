To run locally:

- Download & install the Google AppEngine SDK
- cd to this directory (which contains app.yaml) and `dev_appserver.py --host=0.0.0.0 app.yaml`

To send tasks from the phone locally:
- make sure your phone is on the same wifi network as your computer.
- run `ifconfig` on your computer and find the IP of the computer you're on.
- in the phone app code, find where the hostname is set (currently in
  uploadTask() in utils.js) and set it to that hostname, using port 8080
- deploy app to phone and post
- dev admin console visible at localhost:8000

To deploy the app to appengine proper:

    cd ~<dev-dir>/plural/webapp/
    appcfg.py --oauth2 -A plural-960 update plural/

#!/bin/sh

rm -rf plugins && rm -rf platforms

cordova platform add android
cordova platform add ios

cordova plugin add https://github.com/litehelpers/Cordova-sqlite-storage.git
cordova plugin add cordova-plugin-geolocation
cordova plugin add cordova-plugin-camera
cordova plugin add cordova-plugin-inappbrowser
cordova plugin add cordova-plugin-whitelist
cordova plugin add cordova-plugin-file-transfer
cordova plugin add cordova-plugin-file
cordova plugin add org.apache.cordova.statusbar
cordova plugin add com.ionic.keyboard

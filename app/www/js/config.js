angular.module('starter.config', [])

.factory('Config', function() {
    return {
        service_hostname: 'http://plural-960.appspot.com'
    }
})

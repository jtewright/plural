angular.module('starter.utils', [])

.factory('Utils', function() {
    return {
        getDateString: function(ts) {
                            var minutes = 1000 * 60;
                            var hours = minutes * 60;
                            var days = hours * 24;
                            var weeks = days * 7;
                            var years = days * 365;

                            var now = new Date();

                            var date = new Date(ts);

                            var difference = (now - date);

                            var minsAgo = Math.round(difference / minutes);
                            var hoursAgo = Math.round(difference / hours);
                            var daysAgo = Math.round(difference / days);
                            var weeksAgo = Math.round(difference / weeks);
                            var yearsAgo = Math.round(difference / years);

                            if (yearsAgo >= 1) {
                              return yearsAgo + "y";
                            }
                            else if (weeksAgo >= 1) {
                              return weeksAgo + "w";
                            }
                            else if (daysAgo >= 1) {
                              return daysAgo + "d";
                            }
                            else if (hoursAgo >= 1) {
                              return hoursAgo + "h";
                            }
                            else if (minsAgo >= 1) {
                              return minsAgo + "m";
                            }
                            else {
                              return "1m"
                            }
                       },
        sqlRowsAsArray: function(res) {
                    return _.map(_.range(res.rows.length), function(i) {
                        return JSON.parse(JSON.stringify(res.rows.item(i)));
                    });
                },
        tId: function() {
              function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                  .toString(16)
                  .substring(1);
              }
              return 't' + s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        },
        eId: function() {
              function s4() {
                return Math.floor((1 + Math.random()) * 0x10000)
                  .toString(16)
                  .substring(1);
              }
              return 'e' + s4() + s4() + '-' + s4() + '-' + s4() + '-' +
                s4() + '-' + s4() + s4() + s4();
        }
    }
    });

angular.module('starter', ['ionic', 'ngCordova', 'ngOpenFB', 'starter.controllers', 'starter.services', 'monospaced.elastic', 'focusMe', 'ion-affix', 'uiGmapgoogle-maps'])

.run(function($ionicPlatform, ngFB) {
  ngFB.init({appId: '478193915690433'});

  $ionicPlatform.ready(function() {

    window.addEventListener('native.keyboardshow', function(){
      document.body.classList.add('keyboard-open');
    });

    if(window.cordova){
            cordova.plugins && cordova.plugins.Keyboard && cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
            cordova.plugins.Keyboard.disableScroll(true);
        }

  });
})

.config(function($compileProvider){
  $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|content|file|tel):/);
})

.config(function($ionicConfigProvider) {
  $ionicConfigProvider.navBar.alignTitle('center');
  $ionicConfigProvider.platform.android.tabs.position('bottom');
})

.config(function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        //    key: 'your api key',
        v: '3.17',
        libraries: 'places'
    });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive

  .state('app', {
    url: "/app",
    abstract: true,
    templateUrl: "templates/side-menu.html"
  })

  // Each tab has its own nav history stack:

  .state('app.newSuggestion', {
    cache: false,
    url: '/newSuggestion',
    views: {
      'mainView': {
        templateUrl: 'templates/new-suggestion.html',
        controller: 'NewSuggestionCtrl'
      }
    }
  })

  .state('app.newSuggestionCompose', {
    cache: false,
    url: '/newSuggestionCompose',
    views: {
      'mainView': {
        templateUrl: 'templates/new-suggestion-compose.html',
        controller: 'NewSuggestionComposeCtrl'
      }
    }
  })

  .state('app.hot', {
    cache: true,
    url: '/exploreHot',
    views: {
      'mainView': {
        templateUrl: 'templates/explore-hot.html',
        controller: 'ExploreHotCtrl'
      }
    }
  })

  .state('app.places', {
    cache: true,
    url: '/explorePlaces',
    views: {
      'mainView': {
        templateUrl: 'templates/explore-places.html',
        controller: 'ExplorePlacesCtrl'
      }
    }
  })

  .state('app.stream', {
      cache: true,
      url: '/stream',
      views: {
        'mainView': {
          templateUrl: 'templates/stream.html',
          controller: 'StreamCtrl'
        }
      }
    })

  .state('app.suggestion-detail', {
      cache: false,
      url: '/suggestion-detail',
      views: {
        'mainView': {
          templateUrl: 'templates/suggestion-detail.html',
          controller: 'SuggestionDetailCtrl'
        }
      }
    })

  .state('app.entity-profile', {
      cache: true,
      url: '/entity-profile',
      views: {
        'mainView': {
          templateUrl: 'templates/entity-profile.html',
          controller: 'EntityProfileCtrl'
        }
      }
    })

  .state('app.user-profile', {
      cache: true,
      url: '/user-profile',
      views: {
        'mainView': {
          templateUrl: 'templates/user-profile.html',
          controller: 'UserProfileCtrl'
        }
      }
    })

  .state('app.my-profile', {
      cache: true,
      url: '/my-profile',
      views: {
        'mainView': {
          templateUrl: 'templates/user-profile.html',
          controller: 'UserProfileCtrl'
        }
      }
    })

  .state('app.settings', {
      cache: false,
      url: '/settings',
      views: {
        'mainView': {
          templateUrl: 'templates/settings.html',
          controller: 'SettingsCtrl'
        }
      }
    })

  .state('app.welcome', {
      cache: false,
      url: '/welcome',
      views: {
        'mainView': {
          templateUrl: 'templates/welcome.html',
          controller: 'WelcomeCtrl'
        }
      }
    });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/stream');

});

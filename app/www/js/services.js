angular.module('starter.services', ['starter.utils', 'starter.config'])

.factory('Tasks', function($http, Utils, Config) {

    var target = 'WEB';
    return {
        all: function(userid, following, entity, justuser, cb) {
            var handleSuccess = function(resp) {
                return cb(resp.data);
            }
            var handleError = function(resp) {
                return resp.data;
            }
            var request = $http({
                method: "get",
                url: Config.service_hostname + "/tasks" + "?" + "userid=" + [userid] + "&" + "following=" + [following] + "&" + "entity=" + [entity] + "&" + "justuser=" + justuser
            });
            return (request.then(handleSuccess, handleError));
        },
        listEntities: function (userid, entity_id, cb) {
            var handleSuccess = function(resp) {
                    return cb(resp.data);
                }
                var handleError = function(resp) {
                    return resp.data;
                }
                var request = $http({
                    method: "get",
                    url: Config.service_hostname + "/entities" + "?" + "userid=" + [userid] + "&" + "entity_id=" + entity_id
                });
                return (request.then(handleSuccess, handleError));
        },
        add: function(task, uploaded) {
            this.uploadTask(task, uploaded);
            return true;
            var keys = ['title', 'image_path', 'timestamp',
                'lng', 'lat', 'user', 'id'
            ];
            var values = keys.map(function(k) {
                return task[k]
            });
        },
        delete: function(id, done) {
            var request = $http({
                method: "post",
                url: Config.service_hostname + "/delete?" + [id]
            });
            var handleSuccess = function(resp) {
                return done(resp);
            }
            var handleError = function(resp) {
                return done(resp);
            }
            return (request.then(handleSuccess, handleError));
        },

        uploadTask: function(task, uploaded) {
            if (!window.cordova) {
                return;
            }

            var win = function(r) {
                return uploaded(r.response);
            }

            var fail = function(error) {
                return uploaded(error.body);
            }

            var options = new FileUploadOptions();
            options.fileKey = "image";
            options.httpMethod = "POST";
            options.fileName = "image-file";
            options.mimeType = "image/jpeg";
            options.params = _.omit(task, ['image_path']);

            var ft = new FileTransfer();
            ft.upload(task.image_path,
                encodeURI(Config.service_hostname + "/tasks"),
                win, fail, options);
        },

        Support: function(id, userid, token, end_user_supported, supporttime, supporters) {
            var request = $http({
                method: "get",
                url: Config.service_hostname + "/support"
                + "?" + "id=" + [id]
                + "&" + "userid=" + [userid]
                + "&" + "token=" + [token]
                + "&" + "delete=" + [end_user_supported]
                + "&" + "supporttime=" + [supporttime]
            });
            var handleSuccess = function(resp) {
                return supporters(resp.data);
            }
            var handleError = function(resp) {
                return supporters(resp.data);
            }
            return (request.then(handleSuccess, handleError));
        },

        follow: function(userid, token, id, user_followed, followtime, name, latitude, longitude, vicinity, gPlacesID, follower) {
            var request = $http({
                method: "get",
                url: Config.service_hostname + "/follow" +
                "?" + "userid=" + [userid] +
                "&" + "token=" + [token] +
                "&" + "entityid=" + [id] +
                "&" + "unfollow=" + [user_followed] +
                "&" + "followtime=" + [followtime] +
                "&" + "name=" + [name] +
                "&" + "latitude=" + [latitude] +
                "&" + "longitude=" + [longitude] +
                "&" + "vicinity=" + [vicinity] +
                "&" + "gPlacesID=" + [gPlacesID]
            });
            var handleSuccess = function(resp) {
                return follower(resp.data);
            }
            var handleError = function(resp) {
                return follower(resp.data);
            }
            return (request.then(handleSuccess, handleError));
        },

        listPlaces: function(location, userid, places) {
            var request = $http({
                method: "get",
                url: Config.service_hostname + "/places" + "?" + "userid=" + [userid] + "&" + "location=" + [location]
            });
            var handleSuccess = function(resp) {
                return places(resp.data);
            }
            var handleError = function(resp) {
                return resp.data;
            }
            return (request.then(handleSuccess, handleError));
        },

        searchPlaces: function(location, search, userid, places) {
            var request = $http({
                method: "get",
                url: Config.service_hostname + "/places" + "?" + "userid=" + [userid] + "&" + "location=" + [location] + "&query=" + [search]
            });
            var handleSuccess = function(resp) {
                return places(resp.data);
            }
            var handleError = function(resp) {
                return resp.data;
            }
            return (request.then(handleSuccess, handleError));
        },

        userDetails: function(userid, user) {
            var request = $http({
                method: "get",
                url: Config.service_hostname + "/user" + "?" + "userid=" + [userid]
            });
            var handleSuccess = function(resp) {
                return user(resp.data);
            }
            var handleError = function(resp) {
                return resp.data;
            }
            return (request.then(handleSuccess, handleError));
        },

        tempUser: [],
        tempOtherUser: [],
        tempOtherUserSave: function(otherUserid, userid) {
            tempOtherUser = otherUserid;
            tempUser = userid;
        },
        tempOtherUserGet: function(tempOtherUser, tempUser) {
            return tempOtherUser, tempUser;
        },

        tempTask: {},
        tempSave: function(task, userid) {
            tempTask = task;
            tempUser = userid;
        },
        tempGet: function(tempTask, tempUser) {
            return tempTask.data, tempUser;
        },

        tempEnt: [],
        tempEntSave: function(entity_id, userid) {
            tempEnt = entity_id;
            tempUser = userid
        },
        tempEntGet: function(tempEnt, tempUser) {
            return tempEnt, tempUser;
        },

        addResponse: function(id, responsetext, responsestatus, responsetime) {
            var request = $http({
              method: "post",
              url: Config.service_hostname + "/response" + "?" + "id=" + [id] + "&" + "responsetext=" + [responsetext] + "&" + "responsestatus=" + [responsestatus] + "&" + "responsetime=" + [responsetime]
            });
        }
    }
})

.factory('Camera', ['$q', function($q) {

    return {
    getPicture: function(options) {
      var q = $q.defer();

      navigator.camera.getPicture(function(result) {
        // Do any magic you need
        q.resolve(result);
      }, function(err) {
        q.reject(err);
      }, options);

      return q.promise;
    }
  }

}]);

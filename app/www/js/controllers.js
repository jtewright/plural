angular.module('starter.controllers', ['starter.utils'])

.config(function($compileProvider) {
  $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
})

.controller('PlatformCtrl', function($scope) {

  ionic.Platform.ready(function(){
    // will execute when device is ready, or immediately if the device is already ready.
  ionic.Platform.isFullScreen = true;
  StatusBar.hide();
  });
})

.controller('WelcomeCtrl', function($scope, ngFB, Tasks, $state, $ionicHistory, $ionicSideMenuDelegate) {

  var getFBuser = function() {
    ngFB.api({
        path: '/me',
        params: {fields: 'id,name,email'}
    }).then(
        function (user) {
            $scope.user = user;
        },
        function (error) {
            $state.go('app.stream');
        });
  }
  getFBuser();

  $scope.newSuggestion = function() {
    $state.go('app.newSuggestion');
  }

  $scope.close = function() {
    $ionicHistory.goBack();
  }

})

.controller('SideMenuCtrl', function($scope, ngFB, Tasks, $state, $ionicHistory, $ionicSideMenuDelegate) {

  $scope.$on('$ionicView.enter', function(){
    $scope.currentView = $ionicHistory.currentStateName();

  });

  $scope.myProfileGo = function() {
    ngFB.api({
        path: '/me',
        params: {fields: 'id,name'}
    }).then(
        function (user) {
            $scope.user = user;
            Tasks.tempOtherUserSave($scope.user.id, $scope.user);
            $state.go('app.my-profile');
        },
        function (error) {
        });
  }

})

.controller('NewSuggestionComposeCtrl', function($scope, $ionicModal, $ionicPopup, ngFB, $state, Tasks, Camera, Utils, $ionicSideMenuDelegate, $ionicHistory) {

  ngFB.api({
        path: '/me',
        params: {fields: 'id,name'}
    }).then(
        function (user) {
            $scope.user = user;
        },
        function (error) {
        });

  $scope.task = $scope.task || {};
  $scope.getDateString = Utils.getDateString;
  $scope.uploading = 0;

  var tempGet = function () {
    Tasks.tempGet(tempTask);
    $scope.task = tempTask;
    $scope.task.location = $scope.task.lat + "," + $scope.task.lng;
    if ($scope.task.entity_id) {
    } else {
      $scope.task.entity_id = Utils.eId();
    }
  }
  tempGet();

  var round = function(val, places) {
    var by = Math.pow(10, places);
    return Math.round(val * by) / by;
  }

  var setLocation = function(position) {
    if (!$scope.task.lat) {
      $scope.task.lat = round(position.coords.latitude, 7);
      $scope.task.lng = round(position.coords.longitude, 7);
      $scope.task.location = $scope.task.lat + "," + $scope.task.lng;
    }
  }
  navigator.geolocation.getCurrentPosition(setLocation);

  $scope.cancel = function() {
    $ionicHistory.goBack();
  };

  $scope.clearSearch = function(task) {
    $scope.task.entity_search = null;
  }

  $scope.$on('$ionicView.leave', function(){
    $scope.uploading = 0;
    $scope.task.title = null;
    $scope.task.description = null;
    $scope.task.entity_id = null;
    $scope.lastPhoto = null;
    $scope.task.image_path = null;
  });

  $scope.getPhoto = function() {
    Camera.getPicture({
      quality: 50,
      saveToPhotoAlbum: false,
      cameraOrientation: true,
      sourceType: navigator.camera.PictureSourceType.CAMERA
    }).then(function(imageURI) {
      $scope.lastPhoto = imageURI;
    }, function(err) {
    });
  };

  $scope.getLibraryPhoto = function() {
    Camera.getPicture({
      quality: 50,
      saveToPhotoAlbum: false,
      sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY
    }).then(function(imageURI) {
      $scope.lastPhoto = imageURI;
    }, function(err) {
    });
  };

  $scope.addTask = function(task) {
    $scope.uploading = 1;
    if (task.location) {
      var location = task.location.split(',');
    }
    else {
      var location = 'no location';
    }
    var taskToSave = {
      id: Utils.tId(),
      userid: $scope.user.id,
      username: $scope.user.name,
      title: task.title,
      description: task.description,
      image_path: $scope.lastPhoto,
      lat: $scope.task.lat,
      lng: $scope.task.lng,
      entity_name: task.entity_name,
      entity_id: task.entity_id,
      entity_gPlacesID: task.entity_gPlacesID,
      entity_vicinity: task.entity_vicinity,
      entity_lat: task.entity_lat,
      entity_lng: task.entity_lng,
      supported: 1,
      token: localStorage.fbAccessToken,
    };
    Tasks.add(taskToSave, function(uploaded) {
      if (uploaded == "Not authorised") {
        $ionicPopup.alert({
          title: "Sorry, there was a problem.",
          template: "We were not able to upload your suggestion. Please logout and login again before retrying."
        });
        $state.go('app.stream');
      } else if (uploaded == "Failed") {
        $scope.uploading = 0;
        $ionicPopup.alert({
          title: "Oops! Please try again.",
          template: "We were not able to upload your suggestion. Sorry, it's probably our fault. Please try again."
        });
      } else if (uploaded == "Success") {
        $state.go('app.stream');
      }
    });
  };

  var listPlaces = function(task) {
    Tasks.listPlaces($scope.task.location, 'None', function(places) {
      $scope.places = places;
    });
  }

  $scope.searchPlaces = function(task) {
    $scope.searching = true;
    Tasks.searchPlaces($scope.task.location, $scope.task.entity_search, $scope.user.id, function(places) {
      if (places[0] != null) {
        $scope.places = places;
        $scope.noplaces = false;
        $scope.searching = false;
      }
      else {
        $scope.places = places;
        $scope.noplaces = true;
        $scope.searching = false;
      }
    });
  }

  $ionicModal.fromTemplateUrl('select-entity.html', {
    scope: $scope,
    animation: 'slide-in-up'
    }).then(function(modal) {
    $scope.modal = modal;
  });

  $scope.openModal = function(task) {
    $scope.modal.show();
    if ($scope.task.location) {
      listPlaces(task);
    }
    else {
      var setLocation = function(position) {
        $scope.task.lat = round(position.coords.latitude, 7);
        $scope.task.lng = round(position.coords.longitude, 7);
        $scope.task.location = $scope.task.lat + "," + $scope.task.lng;
        listPlaces(task);
      };
      navigator.geolocation.getCurrentPosition(setLocation);
    }
  }
  $scope.closeModal = function(place) {
    if (place == 'None') {
      $scope.modal.hide();
    }
    else {
      $scope.modal.hide();
      $scope.task.entity_name = place.name;
      $scope.task.entity_gPlacesID = place.gPlacesID;
      if (place.id) {
        $scope.task.entity_id = place.id;
      } else {
        $scope.task.entity_id = Utils.eId();
      }
      if (place.vicinity) {
        $scope.task.entity_vicinity = place.vicinity;
      } else if (place.address) {
        $scope.task.entity_vicinity = place.address;
      }
      $scope.task.entity_lat = place.latitude;
      $scope.task.entity_lng = place.longitude;
      $scope.task.lat = place.latitude;
      $scope.task.lng = place.longitude;
      $scope.task.location = $scope.task.lat + "," + $scope.task.lng;
    }
  };
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  $scope.$on('modal.hidden', function() {
  });
  $scope.$on('modal.removed', function() {
  });

})

.controller('NewSuggestionCtrl', function($scope, $ionicModal, $state, Tasks, Camera, Utils, ngFB, uiGmapGoogleMapApi, $ionicSideMenuDelegate, $ionicHistory) {

  var getFBuser = function() {
    ngFB.api({
        path: '/me',
        params: {fields: 'id,name'}
    }).then(
        function (user) {
            $scope.user = user;
            navigator.geolocation.getCurrentPosition(setLocation);
        },
        function (error) {
        });
  }
  getFBuser();

  $scope.backView = $ionicHistory.backView()

  $scope.cancel = function() {
    $ionicHistory.goBack();
  };

  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.clearSearch = function(task) {
    $scope.task.entity_search = null;
  }

  $scope.getDateString = Utils.getDateString;

  $scope.task = $scope.task || {};
  $scope.task.timestamp = new Date().getTime();

  $scope.uploading = 0;

  var round = function(val, places) {
    var by = Math.pow(10, places);
    return Math.round(val * by) / by;
  }

  var setLocation = function(position) {
    if (!$scope.task.location) {
      $scope.task.lat = round(position.coords.latitude, 7);
      $scope.task.lng = round(position.coords.longitude, 7);
      $scope.task.location = $scope.task.lat + "," + $scope.task.lng;
      listPlaces();
    } else {
      listPlaces();
    }
  }

  $scope.searchPlaces = function(task) {
    $scope.searching = true;
    Tasks.searchPlaces($scope.task.location, $scope.task.entity_search, $scope.user.id, function(places) {
      if (places[0] != null) {
        $scope.places = places;
        $scope.noplaces = false;
        $scope.searching = false;
      }
      else {
        $scope.places = places;
        $scope.noplaces = true;
        $scope.searching = false;
      }
    });
  }

  var listPlaces = function(task) {
    Tasks.listPlaces($scope.task.location, 0, function(places) {
      if (places[0] != null) {
        $scope.places = places;
        $scope.noplaces = false;
      }
      else {
        $scope.places = places;
        $scope.noplaces = true;
      }
    });
  }

  $scope.intoCompose = function(place) {
    $scope.task.entity_name = place.name;
    if (place.gPlacesID) {
      $scope.task.entity_gPlacesID = place.gPlacesID;
    } else {
      $scope.task.entity_gPlacesID = 'None';
    }
    if (place.id) {
      $scope.task.entity_id = place.id;
    }
    if (place.vicinity) {
      $scope.task.entity_vicinity = place.vicinity;
    } else if (place.address) {
      $scope.task.entity_vicinity = place.address;
    } else {
      $scope.task.entity_vicinity = 'None';
    }
    if (place.latitude) {
      $scope.task.entity_lat = place.latitude;
      $scope.task.lat = place.latitude;
    } else {
      $scope.task.entity_lat = 'None';
    }
    if (place.longitude) {
      $scope.task.entity_lng = place.longitude;
      $scope.task.lng = place.longitude;
    } else {
      $scope.task.entity_lng = 'None';
    }
    Tasks.tempSave($scope.task);
    $state.go('app.newSuggestionCompose');
  }

})

.controller('ExploreHotCtrl', function($scope, $state, ngFB, Tasks, Utils, $ionicActionSheet,  uiGmapGoogleMapApi, $ionicSideMenuDelegate, $ionicScrollDelegate, $ionicHistory) {

  var getFBuser = function() {
    ngFB.api({
        path: '/me',
        params: {fields: 'id,name'}
    }).then(
        function (user) {
            $scope.user = user;
            onReadyForRefresh();
        },
        function (error) {
        });
  }

  $scope.$on('$ionicView.enter', function(){
    if (!$scope.user) {
      getFBuser();
    }
    else {
      onReadyForRefresh();
    }
  });

  $scope.suggestionDetail = function(task) {
    Tasks.tempSave(task, $scope.user);
    $state.go('app.suggestion-detail');
  }

  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.newSuggestion = function() {
    $state.go('app.newSuggestion');
  }

  var onReadyForRefresh = function() {
    Tasks.all($scope.user.id, 0, 0, 0, function(rows) {
      $scope.tasks = rows;
    });
  }

  $scope.getDateString = Utils.getDateString;

  $scope.refresh = function () {
    onReadyForRefresh();
  }

})

.controller('ExplorePlacesCtrl', function($scope, $state, $ionicPopup, ngFB, Utils, Tasks, $ionicSideMenuDelegate) {

  var getFBuser = function() {
    ngFB.api({
        path: '/me',
        params: {fields: 'id,name'}
    }).then(
        function (user) {
            $scope.user = user;
            navigator.geolocation.getCurrentPosition(setLocation);
        },
        function (error) {
        });
  }

  $scope.$on('$ionicView.enter', function(){
    if (!$scope.user) {
      getFBuser();
    }
    else {
      navigator.geolocation.getCurrentPosition(setLocation);
    }
  });

  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.newSuggestion = function() {
    $state.go('app.newSuggestion');
  }

  $scope.clearSearch = function(entity) {
    $scope.entity.entity_search = null;
  }

  $scope.entGo = function(entity) {
    Tasks.tempEntSave(entity.id, $scope.user);
    $state.go('app.entity-profile');
  }

  $scope.entity = {};

  $scope.searchPlaces = function(entity) {
    $scope.searching = true;
    Tasks.searchPlaces($scope.location, $scope.entity.entity_search, $scope.user.id, function(places) {
      if (places[0] != null) {
        $scope.entities = places;
        $scope.noentities = false;
        $scope.searching = false;
      }
      else {
        $scope.entities = places;
        $scope.noentities = true;
        $scope.searching = false;
      }
    });
  }

  var round = function(val, places) {
    var by = Math.pow(10, places);
    return Math.round(val * by) / by;
  }

  var setLocation = function(position) {
    if (!$scope.location) {
      $scope.lat = round(position.coords.latitude, 7);
      $scope.lng = round(position.coords.longitude, 7);
      $scope.location = $scope.lat + "," + $scope.lng;
      listPlaces();
    } else {
      listPlaces();
    }
  }

  var listPlaces = function(task) {
    Tasks.listPlaces($scope.location, $scope.user.id, function(places) {
      if (places[0] != null) {
        $scope.entities = places;
        $scope.noentities = false;
      }
      else {
        $scope.entities = places;
        $scope.noentities = true;
      }
    });
  }

  $scope.task = $scope.task || {};

  $scope.intoCompose = function(entity) {
    $scope.task.entity_name = entity.name;
    if (entity.id) {
      $scope.task.entity_id = entity.id;
    }
    if (entity.gPlacesID) {
      $scope.task.entity_gPlacesID = entity.gPlacesID;
    } else {
      $scope.task.entity_gPlacesID = 'None';
    }
    if (entity.vicinity) {
      $scope.task.entity_vicinity = entity.vicinity;
    } else if (entity.address) {
      $scope.task.entity_vicinity = entity.address;
    } else {
      $scope.task.entity_vicinity = 'None';
    }
    if (entity.latitude) {
      $scope.task.entity_lat = entity.latitude;
      $scope.task.lat = entity.latitude;
    } else {
      $scope.task.entity_lat = 'None';
    }
    if (entity.longitude) {
      $scope.task.entity_lng = entity.longitude;
      $scope.task.lng = entity.longitude;
    } else {
      $scope.task.entity_lng = 'None';
    }
    Tasks.tempSave($scope.task);
    $state.go('app.newSuggestionCompose');
  }

  $scope.follow = function(entity) {
    if (!entity.id) {
      entity.id = Utils.eId();
    }
    if (entity.vicinity) {
      vicinity = entity.vicinity;
    } else if (!entity.vicinity) {
      vicinity = entity.address;
    }
    followtime = new Date().getTime();
    Tasks.follow($scope.user.id, localStorage.fbAccessToken, entity.id, entity.user_followed, followtime, entity.name, entity.latitude, entity.longitude, vicinity, entity.gPlacesID, function(follower) {
      if (follower == "Not authorised") {
        $ionicPopup.alert({
          title: 'Sorry, there was a problem.',
          template: 'We were not able let you follow this place. This is almost certainly our fault. Sorry. Please logout and login again.'
        });
      }
      else {
        entity.user_followed = follower.user_followed;
      }
    })
  }

})

.controller('UserProfileCtrl', function($scope, $state, ngFB, Tasks, Utils, $ionicSideMenuDelegate, $ionicHistory, uiGmapGoogleMapApi) {

  $scope.currentView = $ionicHistory.currentStateName();

  $scope.cancel = function() {
    if ($ionicHistory.backView() == null) {
      $state.go('app.stream');
    }
    $ionicHistory.goBack();
  };

  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.newSuggestion = function() {
    $state.go('app.newSuggestion');
  }

  $scope.suggestionDetail = function(task) {
    Tasks.tempSave(task, $scope.user);
    $state.go('app.suggestion-detail');
  }

  $scope.otherUser = $scope.otherUser || {};

  var tempUserGet = function () {
    Tasks.tempOtherUserGet(tempOtherUser, tempUser);
    $scope.otherUser.id = tempOtherUser;
    $scope.user = tempUser;
    Tasks.all($scope.user.id, 0, 0, $scope.otherUser.id, function(rows) {
      $scope.tasks = rows;
    });
    userDetails();
  }

  var userDetails = function() {
    Tasks.userDetails($scope.otherUser.id, function(user) {
      $scope.otherUser = user[0];
    })
  }

  tempUserGet();

})


.controller('EntityProfileCtrl', function($scope, $state, $ionicPopup, ngFB, Tasks, Utils, $ionicSideMenuDelegate, $ionicHistory, uiGmapGoogleMapApi) {

  $scope.cancel = function() {
    if ($ionicHistory.backView() == null) {
      $state.go('app.listProblems');
    }
    $ionicHistory.goBack();
  };

  $scope.newSuggestion = function() {
    $state.go('app.newSuggestion');
  }

  $scope.task = $scope.task || {};

  var entGet = function () {
    Tasks.tempEntGet(tempEnt);
    $scope.entity_id = tempEnt;
    $scope.user = tempUser;
    Tasks.listEntities($scope.user.id, $scope.entity_id, function(entity) {
      $scope.entity = entity[0];
      buildMap();
      buildMarker();
    })
  }
  entGet();

  $scope.follow = function(entity) {
    followtime = new Date().getTime();
    Tasks.follow($scope.user.id, localStorage.fbAccessToken, entity.id, entity.user_followed, followtime, entity.name, entity.latitude, entity.longitude, entity.vicinity, entity.gPlacesID, function(follower) {
      if (follower == "Not authorised") {
        $ionicPopup.alert({
          title: 'Sorry, there was a problem.',
          template: 'We were not able let you follow this place. This is almost certainly our fault. Sorry. Please logout and login again.'
        });
      }
      else {
        entity.user_followed = follower.user_followed;
      }
    })
  }

  $scope.intoCompose = function(entity) {
    $scope.task.entity_name = entity.name;
    if (entity.id) {
      $scope.task.entity_id = entity.id;
    }
    if (entity.gPlacesID) {
      $scope.task.entity_gPlacesID = entity.gPlacesID;
    } else {
      $scope.task.entity_gPlacesID = 'None';
    }
    if (entity.vicinity) {
      $scope.task.entity_vicinity = entity.vicinity;
    } else if (entity.address) {
      $scope.task.entity_vicinity = entity.address;
    } else {
      $scope.task.entity_vicinity = 'None';
    }
    if (entity.latitude) {
      $scope.task.entity_lat = entity.latitude;
      $scope.task.lat = entity.latitude;
    } else {
      $scope.task.entity_lat = 'None';
    }
    if (entity.longitude) {
      $scope.task.entity_lng = entity.longitude;
      $scope.task.lng = entity.longitude;
    } else {
      $scope.task.entity_lng = 'None';
    }
    Tasks.tempSave($scope.task);
    $state.go('app.newSuggestionCompose');
  }

  $scope.suggestionDetail = function(task) {
    Tasks.tempSave(task, $scope.user);
    $state.go('app.suggestion-detail');
  }

  Tasks.all($scope.user.id, 0, $scope.entity_id, 0, function(rows) {
    $scope.tasks = rows;
    // $scope.digest();
  });

  var buildMap = function () {
    $scope.map = {
      center: {
        latitude: $scope.entity.latitude,
        longitude: $scope.entity.longitude },
      zoom: 15,
      control: {},
      options: {
        disableDefaultUI: true,
        styles: [
        {
          "featureType": "poi.park",
          "stylers": [
            { "color": "#18d698" }
          ]
        },{
          "featureType": "road",
          "elementType": "geometry.stroke",
          "stylers": [
            { "color": "#16a8c6" }
          ]
        },{
          "featureType": "poi.school",
          "elementType": "geometry.fill",
          "stylers": [
            { "color": "#18d698" }
          ]
        },{
          "featureType": "landscape.natural",
          "elementType": "geometry",
          "stylers": [
            { "color": "#18d698" }
          ]
        },{
          "featureType": "landscape.man_made",
          "stylers": [
            { "color": "#f0f0f0" }
          ]
        },{
          "featureType": "road",
          "stylers": [
            { "color": "#ffffff" }
          ]
        },{
          "featureType": "road",
          "elementType": "labels.text.fill",
          "stylers": [
            { "color": "#16a8c6" }
          ]
        },{
          "featureType": "water",
          "stylers": [
            { "color": "#16a8c6" }
          ]
        },{
          "featureType": "poi.attraction"  },{
          "elementType": "labels.text.fill",
          "stylers": [
            { "color": "#16a8c6" }
          ]
        },{
          "elementType": "labels.text.stroke",
          "stylers": [
            { "visibility": "on" },
            { "color": "#ffffff" }
          ]
        },{
        },{
          "elementType": "geometry.stroke",
          "stylers": [
            { "color": "#ffffff" },
            { "visibility": "off" }
          ]
        },{
          "elementType": "labels.icon"  },{
          "featureType": "poi.sports_complex",
          "elementType": "labels.icon"  },{
        }
      ]
      }
      }
    }

  var pinicon = new google.maps.MarkerImage(
    "img/location.png",
    null, /* size is determined at runtime */
    null, /* origin is 0,0 */
    null, /* anchor is bottom center of the scaled image */
    new google.maps.Size(21, 34)
  );

  var buildMarker = function () {
    $scope.marker = {
      id: 0,
      coords: {
        latitude: $scope.entity.latitude,
        longitude: $scope.entity.longitude },
      options: {
        draggable: false,
      },
      icon: pinicon
      }
  }

})

.controller('SuggestionDetailCtrl', function($scope, uiGmapGoogleMapApi, $ionicPopup, $timeout, $cordovaClipboard, $ionicHistory, $rootScope, $state, ngFB, Tasks, Utils, $ionicScrollDelegate, $ionicActionSheet, $ionicSideMenuDelegate) {

  $scope.getDateString = Utils.getDateString;

  $scope.newSuggestion = function() {
    $state.go('app.newSuggestion');
  }

  $scope.entGo = function(task) {
    Tasks.tempEntSave(task.entity_id, $scope.user);
    $state.go('app.entity-profile');
  }

  $scope.userGo = function(task) {
    Tasks.tempOtherUserSave(task.userid, $scope.user);
    $state.go('app.user-profile');
  }

  $scope.cancel = function() {
    if ($ionicHistory.backView() == null) {
      $state.go('app.listProblems');
    }
    $ionicHistory.goBack();
  };

  $scope.Support = function(task) {
    supporttime = new Date().getTime();
    Tasks.Support(task.id, $scope.user.id, localStorage.fbAccessToken, task.end_user_supported, supporttime, function(supporters) {
      if (supporters == "Not authorised") {
        $ionicPopup.alert({
          title: 'Sorry, there was a problem.',
          template: 'We were not able to add your support to this suggestion. This is almost certainly our fault. Sorry. Please logout and login again.'
        });
      }
      else if (task.end_user_supported == 1) {
        task.total_supporters = task.total_supporters - 1;
        task.end_user_supported = 0;
      }
      else if (task.end_user_supported == 0) {
        task.total_supporters = task.total_supporters + 1;
        task.end_user_supported = 1;
      }
    });
   }

   $scope.copyText = function(task) {
        var shareURL = "http://www.plural.co/s/?id=" + task.id;
        $cordovaClipboard.copy(shareURL).then(function() {
          $scope.copied = true;
          $timeout(function () {
            $scope.copied = false;
          }, 2000);
        }, function() {
            console.error("Error copying text");
        });
    }

  var tempGet = function () {
    Tasks.tempGet(tempTask, tempUser);
    $scope.task = tempTask;
    $scope.user = tempUser;
  }

  var buildMap = function () {
    $scope.map = {
      center: {
        latitude: $scope.task.latitude,
        longitude: $scope.task.longitude },
      zoom: 15,
      control: {},
      options: {
        disableDefaultUI: true,
        styles: [
        {
          "featureType": "poi.park",
          "stylers": [
            { "color": "#18d698" }
          ]
        },{
          "featureType": "road",
          "elementType": "geometry.stroke",
          "stylers": [
            { "color": "#16a8c6" }
          ]
        },{
          "featureType": "poi.school",
          "elementType": "geometry.fill",
          "stylers": [
            { "color": "#18d698" }
          ]
        },{
          "featureType": "landscape.natural",
          "elementType": "geometry",
          "stylers": [
            { "color": "#18d698" }
          ]
        },{
          "featureType": "landscape.man_made",
          "stylers": [
            { "color": "#f0f0f0" }
          ]
        },{
          "featureType": "road",
          "stylers": [
            { "color": "#ffffff" }
          ]
        },{
          "featureType": "road",
          "elementType": "labels.text.fill",
          "stylers": [
            { "color": "#16a8c6" }
          ]
        },{
          "featureType": "water",
          "stylers": [
            { "color": "#16a8c6" }
          ]
        },{
          "featureType": "poi.attraction"  },{
          "elementType": "labels.text.fill",
          "stylers": [
            { "color": "#16a8c6" }
          ]
        },{
          "elementType": "labels.text.stroke",
          "stylers": [
            { "visibility": "on" },
            { "color": "#ffffff" }
          ]
        },{
        },{
          "elementType": "geometry.stroke",
          "stylers": [
            { "color": "#ffffff" },
            { "visibility": "off" }
          ]
        },{
          "elementType": "labels.icon"  },{
          "featureType": "poi.sports_complex",
          "elementType": "labels.icon"  },{
        }
      ]
      }
      }
  }

  var pinicon = new google.maps.MarkerImage(
    "img/location.png",
    null, /* size is determined at runtime */
    null, /* origin is 0,0 */
    null, /* anchor is bottom center of the scaled image */
    new google.maps.Size(21, 34)
  );

  var buildMarker = function () {
    $scope.marker = {
      id: 0,
      coords: {
        latitude: $scope.task.latitude,
        longitude: $scope.task.longitude },
      options: {
        draggable: false,
      },
      icon: pinicon
      }
  }

  tempGet();
  buildMap();
  buildMarker();

})

.controller('StreamCtrl', function($scope, $rootScope, $state, $ionicPopup, $cordovaClipboard, $timeout, $ionicModal, $state, ngFB, Tasks, Utils, $ionicScrollDelegate, $ionicActionSheet, $ionicSideMenuDelegate, $ionicHistory) {

  var getFBuser = function() {
    ngFB.api({
        path: '/me',
        params: {fields: 'id,name,email'}
    }).then(
        function (user) {
            $scope.user = user;
            closeModal();
            onReadyForRefresh();
        },
        function (error) {
            openModal();
        });
  }

  $scope.fbLogin = function () {
    ngFB.login({scope: 'email'}).then(
        function (response) {
            if (response.status === 'connected') {
                $state.go('app.welcome');
                closeModal();
            } else {
            }
        });
  };

  $scope.$on('$ionicView.enter', function(){
    if (!$scope.user) {
      getFBuser();
    }
    else {
      onReadyForRefresh();
    }
  });

  $rootScope.entityuser = { checked: false };

  $scope.task = $scope.task || {};

  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.newSuggestion = function() {
    $state.go('app.newSuggestion');
  }

  $scope.exploreGo = function() {
    $state.go('app.places');
  }

  $scope.entGo = function(task) {
    Tasks.tempEntSave(task.entity_id, $scope.user);
    $state.go('app.entity-profile');
  }

  $scope.userGo = function(task) {
    Tasks.tempOtherUserSave(task.userid, $scope.user);
    $state.go('app.user-profile');
  }

  $scope.suggestionDetail = function(task) {
    Tasks.tempSave(task, $scope.user);
    $state.go('app.suggestion-detail');
  }

  $scope.getDateString = Utils.getDateString;

  var onReadyForRefresh = function() {
    Tasks.all($scope.user.id, 1, 0, 0, function(rows) {
      window.localStorage['tasks'] = JSON.stringify(rows);
      $scope.tasks = JSON.parse(window.localStorage['tasks']);
    });
  }

  $scope.Support = function(task) {
    supporttime = new Date().getTime();
    Tasks.Support(task.id, $scope.user.id, localStorage.fbAccessToken, task.end_user_supported, supporttime, function(supporters) {
      if (supporters == "Not authorised") {
        $ionicPopup.alert({
          title: 'Sorry, there was a problem.',
          template: 'We were not able to add your support to this suggestion. This is almost certainly our fault. Sorry. Please logout and login again.'
        });
      } else if (task.end_user_supported == 1) {
        task.total_supporters = task.total_supporters - 1;
        task.end_user_supported = 0;
      } else if (task.end_user_supported == 0) {
        task.total_supporters = task.total_supporters + 1;
        task.end_user_supported = 1;
      }
    });
   }

   $scope.copyText = function(task) {
        var shareURL = "http://www.plural.co/s/?id=" + task.id;
        $cordovaClipboard.copy(shareURL).then(function() {
          $scope.copied = true;
          $timeout(function () {
            $scope.copied = false;
          }, 2000);
        }, function() {
            console.error("Error copying text");
        });
    }

   $ionicModal.fromTemplateUrl('first-run.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modal = modal;
  });
  var openModal = function() {
    $scope.modal.show();
  };
  var closeModal = function() {
    $scope.modal.hide();
  };
  //Cleanup the modal when we're done with it!
  $scope.$on('$destroy', function() {
    $scope.modal.remove();
  });
  // Execute action on hide modal
  $scope.$on('modal.hidden', function() {
    // Execute action
  });
  // Execute action on remove modal
  $scope.$on('modal.removed', function() {
    // Execute action
  });

})

.controller('SettingsCtrl', function($scope, $state, Tasks, $rootScope, ngFB, $ionicSideMenuDelegate) {


  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.fbLogin = function () {
    ngFB.login({scope: 'email'}).then(
        function (response) {
            if (response.status === 'connected') {
                loadUser();
                $scope.loggedIn = 1;
            } else {
            }
        });
  };

  $scope.fbLogout = function () {
    ngFB.logout()
    localStorage.removeItem('fbAccessToken');
    $scope.loggedIn = 0;
  }

  var loadUser = function () {
    ngFB.api({
        path: '/me',
        params: {fields: 'id,name,email'}
    }).then(
        function (user) {
            $scope.user = user;
        },
        function (error) {
        });
  }

  if (localStorage.fbAccessToken) {
    loadUser();
    $scope.loggedIn = 1;
  }
  else {
    $scope.loggedIn = 0;
  }

  $scope.task = $scope.task || {};

  $scope.pluralCompose = function() {
    $scope.task.entity_name = "Plural";
    $scope.task.entity_id = "plural";
    $scope.task.entity_gPlacesID = "plural";
    $scope.task.entity_vicinity = "App";
    $scope.task.entity_lat = 37.7703500;
    $scope.task.lat = 37.7703500;
    $scope.task.entity_lng = -122.4506810;
    $scope.task.lng = -122.4506810;
    Tasks.tempSave($scope.task);
    $state.go('app.newSuggestionCompose');
  }


});

angular.module('focusMe', [])

.directive('focusMe', function($timeout) {
  return {
    link: function(scope, element, attrs) {
      $timeout(function() {
        element[0].focus(); 
        if(ionic.Platform.isAndroid()){
           cordova.plugins.Keyboard.show();
        }
      }, 1500);
    }
  };
});